create Database GModell;
	use GModell;
    
    /** Maschinenbauer tabelle **/
	create Table Maschinenbauer(
		Maschinenbauernummer int primary key,
        Name text,
        Nachname text,
        Telefonnummer int        
    );
    
    
    /** Servicetechniker tabelle **/
    create Table Servicetechniker(
		ArbeiterID int primary key,
        Name text,
        Nachname text
    );
    
 
    
    
    /** Kunde Tabelle **/
    create Table Kunde(
		KundenID int primary key,
        Name text,
        Nachname text,
        Maschinenbauernummer int references Maschinenbauer(Maschinenbauernummer)
    );
    

    
    /** Maschine tabelle **/
    create Table Maschine(
		Maschinennummer int primary key,
        Maschinenart text,
        Maschinenname text,
        KundenID int references Kunde(KundenID),
        ArbeiterID int references Servicetechniker(ArbeiterID)
    );
   
    /** Kundentabelle vervollständigen **/
    alter Table Kunde
		add column Maschinennummer int references Maschinen(Maschinennummer) after Maschinenbauernummer;
        
	/** Ersatzteil tabelle **/
	create Table Ersatzteil(
		ErsatzteilID int primary key,
        Ersatzteilname text,
        Maschinennummer int references Maschine(Maschinennummer)
    );
    
    
    /** füge Werte für den Maschinenbauer hinzu **/
	insert into Maschinenbauer(Maschinenbauernummer,Name,Nachname,Telefonnummer)
		value
        (1, "Peter", "Bauer",01010101),
        (2,"Joseph","Müller",02020202);
    
	/** füger Werte für den Servicetechniker hinzu **/
   insert into Servicetechniker(ArbeiterID, Name,Nachname)
		value
        (1,"Patrick","Kröller"),
        (2,"Ulrich", "Sturmmantel");
    
    /** füge Werte für den Kunden hinzu **/
	insert into Kunde(KundenID,Name,Nachname,Maschinenbauernummer)
		value
        (1, "Florian", "Wollweber",(Select Maschinenbauernummer from Maschinenbauer where Maschinenbauernummer=1)),
		(2, "Mark", "Muhr",(Select Maschinenbauernummer from Maschinenbauer where Maschinenbauernummer=2));
        
	/** füge Werte für die Maschine hinzu **/
	insert into Maschine(Maschinennummer,Maschinenart, Maschinenname, KundenID,ArbeiterID)
		value
        (1,"Arbeitsmaschine", "Bohrmaschine", (Select KundenID from Kunde where KundenID=1), (Select ArbeiterID from Servicetechniker where ArbeiterID=1)),
		(2,"Kraftmaschine", "Motor", (Select KundenID from Kunde where KundenID=2), (Select ArbeiterID from Servicetechniker where ArbeiterID=2));
        
	/** füge Werte für das Eratzteil hinzu **/
	insert into Ersatzteil(ErsatzteilID, Ersatzteilname,Maschinennummer)
		value
        (1,"Ersatzteil1",(Select Maschinennummer from Maschine where Maschinennummer=1)),
        (2,"Ersatzteil2", (Select Maschinennummer from Maschine where Maschinennummer=2));
    
	update Kunde set Maschinennummer=(Select Maschinennummer from Maschine where Maschinennummer=1) where KundenID=1;
    update Kunde set Maschinennummer=(Select Maschinennummer from Maschine where Maschinennummer=2) where KundenID=2;