function convert(unicodeCodepoints cp[]){
	String result = "";
	for(unicodeCodepoint x : cp){

		if(x entspricht nicht der erwarteten Eingabe){	//erwartete Eingabe Bsp. = "U+0041"
			result = "Mindestens ein unicode Codepoint wurde Fehlerhaft eingegeben!";
		}else{
		
			String filteredCode = getLastTwoDigits(x); // filter die letzten beiden character aus dem codepoint
			int asInt = parseInt(filteredCode); // parse den String zu einem integer
			int asHex = parseInt(asInt,16); // parse den integer zu einer hexadezimalzahl
	
			String result += asHex.toString(2); // wandle die hexadezimalzahl zu einer String aus binärzahlen
		}

	}
	return result;
}