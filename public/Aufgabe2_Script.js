/** Die Funktion entnimmt den input der entsprechenden Felder, sofern dies möglich ist
 *
 * @param id der entsprechenden Textfelder
 */
function fetchValue(id) {
  var element = document.getElementById(id);
  return element.value;
}

/** Die Funktion konvertiert die unicode Codepoints zu UTF-8 binärcodes und nutzt DOM-Manipulation um diese in der HTML sichtbar zu machen */
function A() {
  // Eingaben des Client als String
  var firstInput = fetchValue("input1");
  var secondInput = fetchValue("input2");
  var thirdInput = fetchValue("input3");
  var fourthInput = fetchValue("input4");

  // Unicodes als Dezimalzahl
  var int1;
  var int2;
  var int3;
  var int4;

  // Dezimalzahlen als Binärzahlen
  int1 = parseInt(firstInput.substring(4, 6), 16);
  int2 = parseInt(secondInput.substring(4, 6), 16);
  int3 = parseInt(thirdInput.substring(4, 6), 16);
  int4 = parseInt(fourthInput.substring(4, 6), 16);

  // Dezimalzahlen als Binärstring
  firstInput = int1.toString(2);
  secondInput = int2.toString(2);
  thirdInput = int3.toString(2);
  fourthInput = int4.toString(2);
  // Um UTF-8 Binärzahlen zu erhalten muss eine 0 zu Anfang jedes Binärcodes angehängt werden.

  var result = "";
  //überprüfe ob die variablen erfolgreich konvertiert wurden
  if (firstInput != "NaN") {
    result += "0" + firstInput;
  }
  if (secondInput != "NaN") {
    result += " 0" + secondInput;
  }
  if (thirdInput != "NaN") {
    result += " 0" + thirdInput;
  }
  if (fourthInput != "NaN") {
    result += " 0" + fourthInput;
  }

  // Neues Element per DOM-Manipulation an die HTML "hängen"
  var child = document.createElement("p");
  child.innerHTML = result;
  document.getElementById("parent").appendChild(child);
}
